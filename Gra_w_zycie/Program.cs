﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gra_w_zycie
{
    class Program
    {
        static int ROZMIAR = 10;
        static int MAX= ROZMIAR + 2;
        static int ZYWE = 60;


        static void print(char[,] map)
        {
            for (int i = 1; i < ROZMIAR + 1; ++i)
            {
                for (int k = 1; k < ROZMIAR + 1; ++k)
                {
                    Console.Write(map[i, k]);
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }

        static void init(char[,] map)
        {
            for (int i = 0; i < MAX; ++i)
            {
                for (int k = 0; k < MAX; ++k)
                {
                    map[i, k] = '.';
                }
            }
            Random random = new Random();
            for (int i=0; i < ZYWE; ++i)
            {
                int a = random.Next(0, ROZMIAR)+1;
                int b = random.Next(0, ROZMIAR)+1;
                if (map[a, b] == '*')
                    --i;
                map[a,b] = '*';
            }
        }

        static uint HowManyNeg(char[,] map, int i, int k)
        {
            uint output = 0;
            
            for (int j = -1; j < 2; ++j)
            {
                if (map[i + j, k + 1] == '*') 
                    ++output;
                if (map[i + j, k - 1] == '*')
                    ++output;
            }
            if (map[i - 1 , k] == '*')
                ++output;
            if (map[i + 1, k] == '*')
                ++output;

            return output;
        }

        static void changeMap(char[,] map)
        {
            char[,] tmp = new char[MAX, MAX];
            init(tmp);
            for (int i = 1; i < MAX - 1; ++i)
            {
                for (int k = 1; k < MAX - 1; ++k)
                {
                    uint neg = HowManyNeg(map, i, k);
                    if (map[i,k] == '*')
                    {
                        if (neg < 2)
                            tmp[i, k] = '.';
                        else if (neg > 3)
                            tmp[i, k] = '.';
                        else 
                            tmp[i, k] = '*';
                    }
                    else
                    {
                        if (neg < 3)
                            tmp[i, k] = '.';
                        else if (neg > 3)
                            tmp[i, k] = '.';
                        else
                            tmp[i, k] = '*';
                    }
                }
            }
            for (int i = 0; i < MAX; ++i)
            {
                for (int k = 0; k < MAX; ++k)
                {
                    map[i, k] = tmp[i, k];
                }
            }
        }
 
        static void Main(string[] args)
        { 
            Console.WriteLine("Podaj rozmiar planszy: ");
            ROZMIAR = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj ilość żywych komorek: ");
            ZYWE = int.Parse(Console.ReadLine());
            MAX = ROZMIAR + 2;
            char[,] map = new char[MAX, MAX];
            init(map);
            int i = 0;
            while (true)
            {
                Console.Clear();
                Console.Write("Iteracja pętli nr: "+i.ToString()+ "\r\n");
                ++i;
                print(map);
                changeMap(map);                
                Console.ReadKey();

            }
        }
    }
}
